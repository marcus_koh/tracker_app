package club.casadeballoon.tracker_app;

import club.casadeballoon.tracker_app.R;

import android.app.*;
import android.content.*;
import android.os.*;
import android.view.*;
import android.widget.*;

public class MainActivity extends Activity {


	private final Activity me = this;
	
	
	private void refreshHeartbeat() {
		TextView nameText = (TextView) findViewById(R.id.main_trackernametext);
		nameText.setText(Utils.getStringVal(me, Utils.trackerNameKey, ""));

		TextView heartbeatText = (TextView) findViewById(R.id.main_heartbeattext);
		String heartbeat =  "Phone: " + Utils.getStringVal(me, Utils.pingSMSKey, "") + "\n"
							+ "URL: " + Utils.getStringVal(me, Utils.trackerUrlKey, "") + "\n"
							+ Utils.getStringVal(me, Utils.gpsStatusKey, "") + "\n" 
							+ Utils.getStringVal(me, Utils.smsStatusKey, "") + "\n"
							+ Utils.getStringVal(me, Utils.trackerStatusKey, "") + "\n"
							+ Utils.getStringVal(me, Utils.photoStatusKey, "");
		
		heartbeatText.setText(heartbeat);
	}

	private Boolean isValidClick() {
		refreshHeartbeat();
		CheckBox confirm1 = (CheckBox) findViewById(R.id.main_confirm1);
		CheckBox confirm2 = (CheckBox) findViewById(R.id.main_confirm2);
		if(confirm1.isChecked() && confirm2.isChecked()) {
			confirm1.setChecked(false);
			confirm2.setChecked(false);
			confirm1.setText("Is");
			confirm2.setText("Confirmed");
			return true;
		}
		else {
			confirm1.setChecked(false);
			confirm2.setChecked(false);
			confirm1.setText("Not");
			confirm2.setText("Confirmed");
			return false;
		}
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.main_activity);		
		
		Button start = (Button) findViewById(R.id.main_startbutton);
		start.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(isValidClick()){
					Intent name = new Intent(MainActivity.this, StartActivity.class);
					startActivity(name);
				}
			}
		});

		Button stop = (Button) findViewById(R.id.main_stopbutton);
		stop.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(isValidClick()){
				    Intent name = new Intent(MainActivity.this, StopActivity.class);
					startActivity(name);
				}
			}
		});

		
		Button takePhoto = (Button) findViewById(R.id.main_photobutton);
		takePhoto.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(isValidClick()){
					Intent name = new Intent(MainActivity.this, PhotoActivity.class);
					startActivity(name);
				}
			}
		});

		Button sendSMS = (Button) findViewById(R.id.main_smsbutton);
		sendSMS.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(isValidClick()){
			    	Utils.sendLocationSMS(me);
			    	Utils.sendTrackerPing(me);
				}
			}
		});

		Button reboot = (Button) findViewById(R.id.main_rebootbutton);
		reboot.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(isValidClick()){
					Utils.reboot(me);
				}
			}
		});
		
		Button quit = (Button) findViewById(R.id.main_quitbutton);
		quit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(isValidClick()){
					me.finish();
				}
			}
		});
		
		
		Button loadConfig = (Button) findViewById(R.id.main_configreload);
		loadConfig.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(isValidClick()){
					Utils.loadConfig(me);
					refreshHeartbeat();
				}
			}
		});	
		
	}
	
	@Override
	protected void onResume() {
		refreshHeartbeat();
		super.onResume();
	}
	
	
}

