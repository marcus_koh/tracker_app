package club.casadeballoon.tracker_app;

import java.io.*;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.*;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.util.Log;

public class Utils {

	public static int alarmId = 314;
	public static String photoPrefixKey = "PhotoPrefix";
	public static String photoCountKey = "PhotoCount";
	public static String photoRunningKey = "PhotoRunning";
	public static String errorCountKey = "ErrorCount";
	public static String trackerNameKey = "TrackerName";
	public static String pingSMSKey = "PhoneNumber";
	public static String trackerUrlKey = "TrackerUrl";
	
	public static String smsStatusKey = "SMSStatus";
	public static String gpsStatusKey = "GPSStatus";
	public static String trackerStatusKey = "TrackerStatus";
	public static String photoStatusKey = "PhotoStatus";

	public static String errorLogFile = "CasaDeBalloonTracker.error.log";
	public static String nmeaLogFile = "CasaDeBalloonTracker.nmea.log";
	public static String configFile = "CasaDeBalloonTracker.config";
	
	public static String getStringVal(Context context, String key,
			String defValue) {
		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(context);
		return settings.getString(key, defValue);
	}

	public static void putStringVal(Context context, String key, String val) {
		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(key, val);
		editor.commit();
	}

	public static Boolean getBooleanVal(Context context, String key,
			Boolean defValue) {
		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(context);
		return settings.getBoolean(key, defValue);
	}

	public static void putBooleanVal(Context context, String key, Boolean val) {
		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(key, val);
		editor.commit();
	}

	public static Integer getIntVal(Context context, String key,
			Integer defValue) {
		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(context);
		return settings.getInt(key, defValue);
	}

	public static void putIntVal(Context context, String key, Integer val) {
		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt(key, val);
		editor.commit();
	}

	public static Integer incrementIntVal(Context context, String key) {
		Integer val = getIntVal(context, key, 0);
		val = val + 1;
		putIntVal(context, key, val);
		return val;
	}

	public static String getStorageRoot() {
		return Environment.getExternalStorageDirectory()+"/CasaDeBalloon";
	}
	
	public static void logException(Throwable ex) {
		try {
			PrintWriter pw = new PrintWriter(
					new FileWriter(Utils.getStorageRoot()
							+ "/" + Utils.errorLogFile, true));
			ex.printStackTrace(pw);
			pw.flush();
			pw.close();
		} catch (Throwable e) {
	        Log.e("Exception", "File write failed: " + e.toString());
		}
	}
	
	public static void appendToFile(String filename, String data) {
	    try {
			 PrintWriter pw = new PrintWriter(
					new FileWriter(Utils.getStorageRoot()
							+ "/" + filename, true));
			 pw.print(data);
			 pw.flush();
			 pw.close();
	    }
	    catch (Throwable e) {
	        Log.e("Exception", "File write failed: " + e.toString());
	    } 
	}

	public static void reboot(Context context) {
		try {
			Log.d("CasaDeBalloon", "Utils attempting to reboot via su");
			java.lang.Process proc = Runtime.getRuntime().exec(
					new String[] { "su", "-c", "reboot" });
			proc.waitFor();
		} catch (Throwable ex) {
			Log.d("CasaDeBalloon", "Error attempting to reboot via su");
		}
	}

	public static void pingSuccess(Context context) {
		putIntVal(context, Utils.errorCountKey, 0);
	}

	public static void pingError(Context context) {
		Integer errorCount = incrementIntVal(context, Utils.errorCountKey);
		if (errorCount > 5) {
			putIntVal(context, Utils.errorCountKey, 0);
			reboot(context);
		}
	}

	public static String getLocationString(Location location) {

		String timestamp = new SimpleDateFormat("HH:mm:ss").format(new Date(location.getTime()));

		DecimalFormat df = new DecimalFormat("#.########");
		String latitude = df.format(location.getLatitude());
		String longitude = df.format(location.getLongitude());

		df = new DecimalFormat("#.#");
		String altitude = df.format(location.getAltitude());

		df = new DecimalFormat("#.#");
		String accuracy = df.format(location.getAccuracy());

		return timestamp + " http://maps.google.com/?q=" + latitude + ","
				+ longitude + " Alt:" + altitude + " Acc:" + accuracy;
	}
	
	public static Location getLocation(Context context) {
		LocationManager locationManager = (LocationManager) context
				.getSystemService(Context.LOCATION_SERVICE);
		Location location = locationManager
				.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		return location;
	}

	public static void sendLocationSMS(Context context) {
		Log.d("CasaDeBalloon", "Utils attempting to send location SMS");
		String phoneNumber = Utils.getStringVal(context, Utils.pingSMSKey, "");
		String trackerName = Utils.getStringVal(context, Utils.trackerNameKey, "");
		try {
			Location location = getLocation(context);
			SmsManager sm = SmsManager.getDefault();
			String locationString = Utils.getLocationString(location);
			String battString = Utils.getBatteryString(context);
			String smsBody = trackerName + ": " + locationString + " " + battString;
			sm.sendTextMessage(phoneNumber, null, smsBody, null, null);
			Log.d("CasaDeBalloon", "Utils location SMS sent to "+phoneNumber+" : "+smsBody);
			String smsStatus = "SMS "+new SimpleDateFormat("HH:mm:ss").format(new Date())+" sent to "+phoneNumber;
			Utils.putStringVal(context, Utils.smsStatusKey, smsStatus);
		} catch (Throwable ex) {
			try {
				SmsManager sm = SmsManager.getDefault();
				String locationString = "Unknown location";
				String battString = Utils.getBatteryString(context);
				String smsBody = trackerName + ": " + locationString + " " + battString;
				sm.sendTextMessage(phoneNumber, null, smsBody, null, null);
			}
			catch (Throwable ex2) {
			}
			Log.d("CasaDeBalloon", "Utils error sending location SMS");
		}
	}

	public static void sendTrackerPing(Context context) {
		Log.d("CasaDeBalloon", "Utils attempting to send tracker ping");
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	    StrictMode.setThreadPolicy(policy);
		try {
			Location location = getLocation(context);
			
			String baseUrl = Utils.getStringVal(context, Utils.trackerUrlKey, "");
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
			dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
			
			String dataUrl = baseUrl + "&ts=" + dateFormat.format(new Date(location.getTime()))
								+ "&lat=" + Double.toString(location.getLatitude())
								+ "&lng=" + Double.toString(location.getLongitude())
								+ "&alt=" + Double.toString(location.getAltitude())
								+ "&notes=";

			HttpParams httpParameters = new BasicHttpParams();
			int timeoutConnection = 4000;
			HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
			int timeoutSocket = 4000;
			HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);			
			HttpClient httpClient = new DefaultHttpClient(httpParameters);
			HttpGet request = new HttpGet(dataUrl);
			httpClient.execute(request);
			
			Log.d("CasaDeBalloon", "Utils location tracker ping sent: "+dataUrl);
			
			String trackerStatus = "Tracker "+new SimpleDateFormat("HH:mm:ss").format(new Date())+" posted";
			Utils.putStringVal(context, Utils.trackerStatusKey, trackerStatus);
		} catch (Throwable ex) {
			Log.d("CasaDeBalloon", "Utils error sending tracker ping");
		}
	}

	public static String getBatteryString(Context context) {
		Intent batteryIntent = context.registerReceiver(null, new IntentFilter(
				Intent.ACTION_BATTERY_CHANGED));
		int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
		int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

		double pct = 0.0;
		if (level > 0 && scale > 0) {
			pct = ((double) level / (double) scale) * 100.0d;
		}

		double volts = 0.0;
		int mv = batteryIntent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, 0);
		if (mv > 0) {
			volts = (double) mv / 1000.0d;
		}

		DecimalFormat df = new DecimalFormat("##");
		String spct = df.format(pct);

		df = new DecimalFormat("#.##");
		String svolts = df.format(volts);
		return "BVlt: " + svolts + " BPct:" + spct;
	}
	
	public static void loadConfig(Context context) {
		try {
			String configFileName = Utils.getStorageRoot() + "/" + Utils.configFile;
			BufferedReader reader = new BufferedReader(new FileReader(configFileName));
			
		    String input = null;
		    if ((input = reader.readLine()) != null) {
		    	Utils.putStringVal(context, Utils.trackerNameKey, input);
		    }
		    if ((input = reader.readLine()) != null) {
		    	Utils.putStringVal(context, Utils.pingSMSKey, input);
		    }
		    if ((input = reader.readLine()) != null) {
		    	Utils.putStringVal(context, Utils.trackerUrlKey, input);
		    }
		    reader.close();
		}
		catch (Throwable ex) {}		
	}
}
