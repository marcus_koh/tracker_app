package club.casadeballoon.tracker_app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class StartupReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		if(Utils.getBooleanVal(context, Utils.photoRunningKey, false)) {
			Intent App = new Intent(context, StartActivity.class);
	        App.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	        context.startActivity(App);
		}
	}

}
