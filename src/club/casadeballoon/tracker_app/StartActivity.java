package club.casadeballoon.tracker_app;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class StartActivity extends Activity {

	private final Activity me = this;

	public static int photoIntervalSeconds = 30;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		AlarmManager m_alarmMgr = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
		
		Intent alarmIntent = new Intent(AlarmReceiver.ALARM_ACTION_NAME);
		PendingIntent alarmPI = PendingIntent.getBroadcast(me, Utils.alarmId, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		        
        long intervalMillis = photoIntervalSeconds * 1000l;
        m_alarmMgr.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, intervalMillis, intervalMillis, alarmPI);
        
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        Utils.putStringVal(me, Utils.photoPrefixKey, "CasaDeBalloon_"+timeStamp+"_");
        Utils.putBooleanVal(me, Utils.photoRunningKey, true);
        Utils.putIntVal(me, Utils.photoCountKey, 0);
        
		Intent serviceIntent = new Intent(me, MainService.class);
	    me.startService(serviceIntent);
        
        me.finish();
	}
}
